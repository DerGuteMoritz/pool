(module pool

(make-pool
 pool-available-values
 pool-size
 pool-borrow!
 pool-return!
 pool-add!
 pool-remove!
 call-with-value-from-pool)

"pool-impl.scm"

)
