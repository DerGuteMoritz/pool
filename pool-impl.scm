(import chicken scheme)
(use srfi-18 srfi-69 data-structures)

(define-record-type pool
  (%make-pool mutex cvar values available-values-queue available-values specific)
  pool?
  (mutex pool-mutex)
  (cvar pool-cvar)
  (values pool-values)
  (available-values-queue pool-available-values-queue)
  (available-values pool-available-values pool-available-values-set!)
  (specific pool-specific))

(define-record pool-value ref borrowed?)

(define (have-lock? mutex)
  (eq? (mutex-state mutex) (current-thread)))

(define (with-locked-pool pool thunk)
  (let ((mutex (pool-mutex pool)))
    (dynamic-wind
        (lambda () (mutex-lock! mutex))
        thunk
        (lambda ()
          (when (have-lock? mutex)
            (mutex-unlock! mutex))))))

(define (pool-available-values-add! pool n)
  (pool-available-values-set! pool (+ (pool-available-values pool) n)))

(define (pool-add! pool value)
  (with-locked-pool
   pool
   (lambda ()
     (when (hash-table-exists? (pool-values pool) value)
       (mutex-unlock! (pool-mutex pool))
       (error 'pool-add! "Pool already contains the given value" pool value))
     (let ((value* (make-pool-value value #f)))
       (hash-table-set! (pool-values pool) value value*)
       (queue-add! (pool-available-values-queue pool) value*)
       (pool-available-values-add! pool 1)
       (condition-variable-signal! (pool-cvar pool))))))

(define (make-pool #!optional (values '()))
  (let ((pool (%make-pool (make-mutex)
                          (make-condition-variable)
                          (make-hash-table eq? eq?-hash)
                          (make-queue)
                          0
                          #f)))
    (for-each (lambda (value)
                (pool-add! pool value))
              values)
    pool))

(define (pool-size p)
  (hash-table-size (pool-values p)))

(define-record-printer (pool p out)
  (display "#<pool " out)
  (write (list size: 
               (pool-size p)
               available:
               (pool-available-values p))
         out)
  (display ">" out))

(define (make-pool-error location message pool . args)
  (make-composite-condition
   (make-property-condition 'exn
                            'message message
                            'location location
                            'arguments (cons pool args)
                            'call-chain (get-call-chain))
   (make-property-condition 'pool
                            'instance pool)))

(define (pool-timeout-error . args)
  (abort
   (make-composite-condition
    (apply make-pool-error args)
    (make-property-condition 'timeout))))

(define (pool-borrow! pool #!optional timeout)
  (with-locked-pool
   pool
   (lambda ()
     (if (queue-empty? (pool-available-values-queue pool))
         (if (and timeout (zero? timeout))
             (begin
               (mutex-unlock! (pool-mutex pool))
               (pool-timeout-error
                'pool-borrow!
                "Timeout while waiting for pool value"
                pool))
             (when (mutex-unlock! (pool-mutex pool) (pool-cvar pool) timeout)
               (pool-borrow! pool (and timeout 0))))
         (let ((value (queue-remove! (pool-available-values-queue pool))))
           (pool-value-borrowed?-set! value #t)
           (pool-available-values-add! pool -1)
           (pool-value-ref value))))))

(define (call-with-pool-value location pool value proc)
  (with-locked-pool
   pool
   (lambda ()
     (let ((value* (hash-table-ref/default (pool-values pool) value #f)))
       (unless value*
         (mutex-unlock! (pool-mutex pool))
         (error location "The given value is not part of that pool" pool value))
       (proc value*)))))

(define (pool-return! pool value)
  (call-with-pool-value
   'pool-return!
   pool
   value
   (lambda (value*)
     (queue-add! (pool-available-values-queue pool) value*)
     (pool-available-values-add! pool 1)
     (pool-value-borrowed?-set! value* #f)
     (condition-variable-signal! (pool-cvar pool)))))

(define (pool-remove! pool value)
  (call-with-pool-value
   'pool-remove!
   pool
   value
   (lambda (value*)
     (unless (pool-value-borrowed? value*)
       (mutex-unlock! (pool-mutex pool))
       (error 'pool-remove!
              "The value has to be borrowed before removing it from the pool"
              pool
              value))
     (hash-table-delete! (pool-values pool) value))))

(define (call-with-value-from-pool pool proc #!optional timeout)
  (let ((value (pool-borrow! pool timeout))
        (have-value? #t))
    (handle-exceptions exn
      (begin
        (when have-value?
          (pool-return! pool value))
        (abort exn))
      (receive results (proc value)
        (set! have-value? #f)
        (pool-return! pool value)
        (apply values results)))))
