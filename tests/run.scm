(use pool test)

(let ((p (make-pool '(1 2 3))))
  (test 3 (pool-available-values p))

  (let ((v1 (pool-borrow! p))
        (v2 (pool-borrow! p))
        (v3 (pool-borrow! p)))
    (test 3 (pool-size p))
    (test 0 (pool-available-values p))

    (pool-remove! p v1)
    (test 2 (pool-size p))
    (test 0 (pool-available-values p))

    (pool-return! p v2)
    (test 2 (pool-size p))
    (test 1 (pool-available-values p))

    (pool-return! p v3)
    (test 2 (pool-size p))
    (test 2 (pool-available-values p))

    (test-error (pool-return! p v1)))

  (pool-add! p 1)
  (test 3 (pool-size p))
  (test 3 (pool-available-values p))

  (let ((got-value? #f))
    (call-with-value-from-pool p (lambda (n) (set! got-value? #t)))
    (test-assert got-value?))

  (test 3 (pool-size p))
  (test 3 (pool-available-values p))

  (test 6
        (call-with-value-from-pool p
          (lambda (a)
            (call-with-value-from-pool p
              (lambda (b)
                (call-with-value-from-pool p
                  (lambda (c)
                    (+ a b c)))))))))


(test-assert
 (condition-case
     (pool-borrow! (make-pool) 0)
   ((pool timeout) #t)
   (e () #f)))
